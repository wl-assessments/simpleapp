##### Node app implementation

--- This standalone application is a simple currency conversion tool, which converts USD to INR and INR to USD. It prints a message in console - The NodeJS application is running successfully on port 8000

--- To get the visualization of app: 
    this application is deployed on google cloud platform - https://luminous-slice-416007.ue.r.appspot.com/



##### Setting up EC2 and Jenkins

--- I created an EC2 instance and installed jenkins and git in it
--- Configured gitlab and docker plugins in jenkins
--- Configured gitlab connection with jenkins with the help of Manage Jenkins > Tools configuration.
--- Also configured Docker installation in jenkins



##### Gitlab configuration

--- I created a personal access token.
--- I pushed the nodejs application in gitlab. Created README.md
--- In project integration option, I configured Jenkins connection with the remote EC2 instance.



##### Jenkins configuration

--- In Jenkins, I created new item - wl-assessment3-jenkinsgitlab2, with pipeline option.
--- In configurations, I enabled following options
    1. added gitlab connection
    2. poll SCM configured for every minute refeshing
    3. Trigger build remotely and gave it authentication token with the help of personal access token from gitlab
--- Pipeline code for Jenkinsfile is given below:
```
pipeline {
    agent {
        docker { image 'node:20.11.1-alpine3.19' }
    }

    stages {
        stage('Checkout') {
            steps {
                // Configure GitLab connection
                git branch:'main', credentialsId: 'cb872175-063a-43ce-961b-c002e1f09b0f', url: 'https://gitlab.com/wl-assessments/simpleapp.git'
            }
        }
        stage('Test') {
            steps {
                sh 'node -c app.js' 
            }
        }
        stage('Build') {
            steps {
                sh 'npm install'
                sh 'node app.js'
            }
        }
    }
}
```



##### .gitlab-ci.yml configuration

--- In this file, I have automated the execution of jenkins pipeline everytime there is a change in gitlab repository
--- Code is given below:
```
image: maven:latest

post_script:
    script: curl -X POST http://3.90.243.206:8080/job/wl-assessment4-jenkinsgitlab2/build?token=glpat-4Jyt-49JsuBRyWnfrNom
```



##### jenkins output
--- With the help of this task, we can automate the deployment of pipeline from Jenkins, everytime there is a push commit in the Gitlab repository

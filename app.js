const express=require("express")
const app=express()
const port= process.env.PORT || 8000

app.get('/', (req,res)=>{res.sendFile(__dirname + '/index.html')})

app.listen(port, ()=>console.log("This is a test. The NodeJS application is running successfully on port "+port))
